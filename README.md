# PeachtreeBank

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Application structure
- Core folder contains components and services that are used to run the app.
- Feature folder contains the main features. It's divided by pages and each page has its own components.
- Shared folder has components, pipes, etc to share through the app.
