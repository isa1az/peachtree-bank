module.exports = {
  content: [
    "./src/**/*.{html,ts}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        'body': "url('assets/images/background.jpg')",
      },
      height: {
        '640': '40rem',
      },
      colors: {
        'peach': '#FF6714',
        'white': '#ffffff',
        'agave': '#00859A',
      },
    },
    fontFamily: {
      'body': ['Kanit', '"Open Sans"'],
    },
  },
  plugins: [],
}
