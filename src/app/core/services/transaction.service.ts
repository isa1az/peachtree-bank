import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, map, Observable } from 'rxjs';
import { sumBy } from 'lodash';
import {
  CreditDebitIndicator,
  CurrencyCode,
  NewTransfer,
  Transfer,
  TransfersResponse
} from '../../interfaces/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private readonly _transfers: BehaviorSubject<Transfer[]>;
  public transfers$: Observable<Transfer[]>;

  constructor(private http: HttpClient) {
    this._transfers = new BehaviorSubject<Transfer[]>([]);
    this.transfers$ = this._transfers.asObservable();

    this.loadTransfers().subscribe({
      next: (transfers) => this._transfers.next(transfers),
      error: (error) => this._transfers.error(error),
    });
  }

  private static parseTransfer(rawData: Transfer): Transfer {
    const operator = rawData.transaction.type === 'Salaries' ? 1 : -1;

    rawData.dates.valueDate = new Date(rawData.dates.valueDate);
    rawData.transaction.amountCurrency.amount = operator * rawData.transaction.amountCurrency.amount;

    return rawData;
  }

  private static createTransfer({to, amount}: NewTransfer): Transfer {
    return {
      categoryCode: '#c12020',
      dates: {
        valueDate: new Date(),
      },
      transaction: {
        amountCurrency: {
          amount: -amount,
          currencyCode: CurrencyCode.Eur
        },
        type: 'Online Transfer',
        creditDebitIndicator: CreditDebitIndicator.Dbit
      },
      merchant: {
        name: to,
        accountNumber: 'SI64397745065188826',
      }
    }
  }

  private loadTransfers(): Observable<Transfer[]> {
    return this.http.get<TransfersResponse>('./assets/mock/transactions.json').pipe(
      map(response => response.data.map(TransactionService.parseTransfer)),
    );
  }

  public makeTransfer(transfer: NewTransfer) {
    const transactions = this._transfers.getValue();
    transactions.push(TransactionService.createTransfer(transfer));
    this._transfers.next(transactions);
  }

  public calculateBalance(): Observable<number> {
    return this.transfers$.pipe(
      map(transfers => sumBy(transfers, 'transaction.amountCurrency.amount')),
    );
  }

  public get balance(): number {
    const transfers = this._transfers.getValue();
    return sumBy(transfers, 'transaction.amountCurrency.amount');
  }
}
