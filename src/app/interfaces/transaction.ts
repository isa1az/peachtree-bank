export interface TransfersResponse {
  data: Transfer[];
}

export interface Transfer {
  categoryCode: string;
  dates: Dates;
  transaction: Transaction;
  merchant: Merchant;
}

export interface Dates {
  valueDate: Date;
}

export interface Merchant {
  name: string;
  accountNumber: string;
}

export interface Transaction {
  amountCurrency: AmountCurrency;
  type: string;
  creditDebitIndicator?: CreditDebitIndicator;
}

export interface AmountCurrency {
  amount: number;
  currencyCode: CurrencyCode;
}

export interface NewTransfer {
  from: string;
  to: string;
  amount: number;
}

export enum CurrencyCode {
  Eur = "EUR",
}

export enum CreditDebitIndicator {
  Crdt = "CRDT",
  Dbit = "DBIT",
}
