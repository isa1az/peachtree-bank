export interface Sorting {
  property: string;
  direction: 'asc' | 'desc';
}

export interface SortingProperty {
  property: string;
  label: string;
}
