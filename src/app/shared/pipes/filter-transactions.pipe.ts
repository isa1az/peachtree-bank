import { Pipe, PipeTransform } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Transfer } from '../../interfaces/transaction';
import { Sorting } from '../../interfaces/sorting';
import { orderBy } from 'lodash';

@Pipe({
  name: 'filterTransactions'
})
export class FilterTransactionsPipe implements PipeTransform {

  transform(transfers$: Observable<Transfer[]> | undefined, search: string, sorting?: Sorting): Observable<Transfer[]> | undefined {
    search = search.toLowerCase();

    return transfers$?.pipe(
      map(transfers => FilterTransactionsPipe.filterTransfers(transfers, search)),
      map(transfers => FilterTransactionsPipe.sortTransfers(transfers, sorting)),
    );
  }

  private static filterTransfers(transfers: Transfer[], search: string): Transfer[] {
    return transfers.filter(trx => {
      return trx.merchant?.name.toLowerCase().includes(search) ||
        trx.transaction?.type.toLowerCase().includes(search) ||
        trx.transaction?.amountCurrency?.amount.toString().toLowerCase().includes(search);
    });
  }

  private static sortTransfers(transfers: Transfer[], sorting: Sorting | undefined): Transfer[] {
    if (!sorting) {
      return transfers;
    }

    return orderBy(transfers, sorting.property, sorting.direction);
  }
}
