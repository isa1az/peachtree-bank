import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'merchantLogo'
})
export class MerchantLogoPipe implements PipeTransform {

  transform(value?: string): string {
    const basePath = './assets/icons';

    switch (value) {
      case 'Backbase':
        return `${basePath}/backbase.png`;
      case 'The Tea Lounge':
        return `${basePath}/the-tea-lounge.png`;
      case 'Texaco':
        return `${basePath}/texaco.png`;
      case 'Amazon Online Store':
        return `${basePath}/amazon-online-store.png`;
      case '7-Eleven':
        return `${basePath}/7-eleven.png`;
      case 'H&M Online Store':
        return `${basePath}/h&m-online-store.png`;
      case 'Jerry Hildreth':
        return `${basePath}/jerry-hildreth.png`;
      case 'Lawrence Pearson':
        return `${basePath}/lawrence-pearson.png`;
      case 'Whole Foods':
        return `${basePath}/whole-foods.png`;
      case 'Southern Electric Company':
        return `${basePath}/southern-electric-company.png`;
    }

    return `https://ui-avatars.com/api/?name=${value}&background=random`;
  }

}
