import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MerchantLogoPipe } from './merchant-logo.pipe';
import { FilterTransactionsPipe } from './filter-transactions.pipe';


@NgModule({
  declarations: [
    MerchantLogoPipe,
    FilterTransactionsPipe
  ],
  exports: [
    MerchantLogoPipe,
    FilterTransactionsPipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule {
}
