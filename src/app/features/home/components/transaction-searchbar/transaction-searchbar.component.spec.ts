import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransactionSearchbarComponent } from './transaction-searchbar.component';

describe('TransactionSearchbarComponent', () => {
  let component: TransactionSearchbarComponent;
  let fixture: ComponentFixture<TransactionSearchbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransactionSearchbarComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSearchbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
