import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { NgModel } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'app-transaction-searchbar',
  templateUrl: './transaction-searchbar.component.html',
  styleUrls: ['./transaction-searchbar.component.scss']
})
export class TransactionSearchbarComponent implements OnInit {
  @ViewChild(NgModel, {static: true}) searchModelReference?: NgModel;
  @Output() onSearch = new EventEmitter<string>();

  private _destroyed$ = new Subject<void>();
  public searchModel = '';

  constructor() {
  }

  ngOnInit(): void {
    this.searchModelReference?.valueChanges?.pipe(
      debounceTime(650),
      distinctUntilChanged(),
      takeUntil(this._destroyed$),
    ).subscribe(
      text => this.search(text),
    );
  }

  search(searchText: string = this.searchModel) {
    this.onSearch.emit(searchText);
  }

  ngOnDestroy() {
    this._destroyed$.next();
    this._destroyed$.complete();
  }

  clearSearch() {
    this.searchModel = '';
  }
}
