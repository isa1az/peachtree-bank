import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentTransactionsComponent } from './recent-transactions.component';
import { TransactionService } from '../../../../core/services/transaction.service';
import { FilterTransactionsPipe } from '../../../../shared/pipes/filter-transactions.pipe';

describe('RecentTransactionsComponent', () => {
  let component: RecentTransactionsComponent;
  let fixture: ComponentFixture<RecentTransactionsComponent>;

  beforeEach(async () => {
    const transactionService = {};

    await TestBed.configureTestingModule({
      declarations: [RecentTransactionsComponent, FilterTransactionsPipe],
      providers: [
        {provide: TransactionService, useFactory: () => transactionService},
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentTransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
