import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../../../../core/services/transaction.service';
import { Observable } from 'rxjs';
import { Transfer } from '../../../../interfaces/transaction';
import { Sorting } from '../../../../interfaces/sorting';

@Component({
  selector: 'app-recent-transactions',
  templateUrl: './recent-transactions.component.html',
  styleUrls: ['./recent-transactions.component.scss']
})
export class RecentTransactionsComponent implements OnInit {
  public transfers$?: Observable<Transfer[]>;
  public search = '';
  public sorting: Sorting = {property:'dates.valueDate', direction: 'desc'};

  public sortingProperties = [
    {property: 'dates.valueDate', label: 'Date'},
    {property: 'merchant.name', label: 'Beneficiary'},
    {property: 'transaction.amountCurrency.amount', label: 'Amount'},
  ];

  constructor(private transactionService: TransactionService) {
  }

  ngOnInit(): void {
    this.transfers$ = this.transactionService.transfers$;
  }

  onSearch(searchText: string) {
    this.search = searchText;
  }
}
