import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TransactionSortingComponent } from './transaction-sorting.component';

describe('TransactionSortingComponent', () => {
  let component: TransactionSortingComponent;
  let fixture: ComponentFixture<TransactionSortingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TransactionSortingComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TransactionSortingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get sorting icon', function () {
    component.sorting.direction = 'asc';
    expect(component.sortingIcon).toBe('caret-up-outline');

    component.sorting.direction = 'desc';
    expect(component.sortingIcon).toBe('caret-down-outline');
  });

  it('should select sorting if sorting is not assigned', function () {
    spyOn(component.sortingChange, 'emit')

    component.selectSorting('dates.valueDate');

    expect(component.sorting).toEqual({property: 'dates.valueDate', direction: 'desc'});
    expect(component.sortingChange.emit).toHaveBeenCalled();
  });

  it('should select sorting if sorting is assigned and it has the same property', function () {
    spyOn(component.sortingChange, 'emit')

    component.sorting = {property: 'merchant.name', direction: 'asc'};
    component.selectSorting('merchant.name');

    expect(component.sorting).toEqual({property: 'merchant.name', direction: 'desc'});
    expect(component.sortingChange.emit).toHaveBeenCalled();
  });

  it('should select sorting if sorting is assigned and it has a different property', function () {
    spyOn(component.sortingChange, 'emit')

    component.sorting = {property: 'merchant.name', direction: 'asc'};
    component.selectSorting('dates.valueDate');

    expect(component.sorting).toEqual({property: 'dates.valueDate', direction: 'asc'});
    expect(component.sortingChange.emit).toHaveBeenCalled();
  });
});
