import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Sorting, SortingProperty } from '../../../../interfaces/sorting';
import { isEqual } from 'lodash';

@Component({
  selector: 'app-transaction-sorting',
  templateUrl: './transaction-sorting.component.html',
  styleUrls: ['./transaction-sorting.component.scss']
})
export class TransactionSortingComponent implements OnInit {
  @Output() sortingChange = new EventEmitter<Sorting>();
  @Input() sortingProperties: SortingProperty[] = [];
  @Input() sorting: Sorting = {property: '', direction: 'desc'};

  constructor() {
  }

  ngOnInit(): void {
  }

  selectSorting(property: string) {
    const direction = this.sorting.property !== property ?
      this.sorting.direction :
      this.sorting.direction === 'desc' ? 'asc' : 'desc';

    const newValue: Sorting = {property, direction};

    if (!isEqual(this.sorting, newValue)) {
      this.sorting = newValue;
      this.sortingChange.emit(this.sorting);
    }
  }

  get sortingIcon() {
    return this.sorting.direction === 'asc' ? 'caret-up-outline' : 'caret-down-outline';
  }
}
