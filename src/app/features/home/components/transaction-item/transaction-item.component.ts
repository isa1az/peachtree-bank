import { Component, Input, OnInit } from '@angular/core';
import { Transfer } from '../../../../interfaces/transaction';

@Component({
  selector: 'app-transaction-item',
  templateUrl: './transaction-item.component.html',
  styleUrls: ['./transaction-item.component.scss']
})
export class TransactionItemComponent implements OnInit {
  @Input() transfer?: Transfer;

  constructor() {
  }

  ngOnInit(): void {
  }
}
