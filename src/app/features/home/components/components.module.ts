import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { RecentTransactionsComponent } from './recent-transactions/recent-transactions.component';
import { NewTransferComponent } from './new-transfer/new-transfer.component';
import { TransactionItemComponent } from './transaction-item/transaction-item.component';
import { TransactionSearchbarComponent } from './transaction-searchbar/transaction-searchbar.component';
import { SharedModule } from '../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TransactionSortingComponent } from './transaction-sorting/transaction-sorting.component';
import { IonicModule } from '@ionic/angular';
import { CurrencyMaskModule } from 'ng2-currency-mask';


@NgModule({
  declarations: [
    RecentTransactionsComponent,
    NewTransferComponent,
    TransactionItemComponent,
    TransactionSearchbarComponent,
    TransactionSortingComponent
  ],
  exports: [
    NewTransferComponent,
    RecentTransactionsComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CurrencyMaskModule,
  ],
  providers: [
    CurrencyPipe,
  ],
})
export class ComponentsModule {
}
