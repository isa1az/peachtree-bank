import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTransferComponent } from './new-transfer.component';
import { TransactionService } from '../../../../core/services/transaction.service';
import { CurrencyPipe } from '@angular/common';
import { of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';

describe('NewTransferComponent', () => {
  let component: NewTransferComponent;
  let fixture: ComponentFixture<NewTransferComponent>;

  beforeEach(async () => {
    const transactionService = {
      calculateBalance: () => of(5000),
    };
    const currencyPipe = {};

    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [NewTransferComponent],
      providers: [
        {provide: TransactionService, useFactory: () => transactionService},
        {provide: CurrencyPipe, useFactory: () => currencyPipe},
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTransferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
