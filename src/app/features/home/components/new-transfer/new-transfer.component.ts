import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TransactionService } from '../../../../core/services/transaction.service';
import { NewTransfer } from '../../../../interfaces/transaction';
import { map } from 'rxjs';
import { CurrencyPipe } from '@angular/common';

@Component({
  selector: 'app-new-transfer',
  templateUrl: './new-transfer.component.html',
  styleUrls: ['./new-transfer.component.scss']
})
export class NewTransferComponent implements OnInit {
  public transferForm: FormGroup;
  public currencyOptions;
  public newTransfer?: NewTransfer;

  constructor(private transactionService: TransactionService, private currencyPipe: CurrencyPipe) {
    this.currencyOptions = {align: 'left', allowNegative: true};

    this.transferForm = new FormGroup({
      from: new FormControl('', [Validators.required]),
      to: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      amount: new FormControl(null, [Validators.required, Validators.maxLength(150)]),
    });
  }

  ngOnInit(): void {
    this.resetForm();

    this.transactionService.calculateBalance().pipe(
      map(balance => `Free Checking(4692) - ${this.currencyPipe.transform(balance)}`),
    ).subscribe({
      next: from => this.transferForm.patchValue({from})
    })
  }

  onSubmit() {
    const { amount } = this.transferForm.value;

    if (this.transactionService.balance - amount > -500) {
      this.newTransfer = this.transferForm.value;
    } else {
      this.transferForm.setErrors({'balance': true});
    }
  }

  onEdit() {
    this.newTransfer = undefined;
  }

  private resetForm() {
    this.newTransfer = undefined;

    this.transferForm.patchValue({
      to: '',
      amount: null,
    });
  }

  makeTransfer() {
    this.transactionService.makeTransfer(this.newTransfer!);
    this.resetForm();
  }
}
